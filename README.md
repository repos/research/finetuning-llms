# Fine tuning LMMs

Quick example on how fine tuning a LMMs models using inputs from the Wikimedia Data Lake

This code is based on the _[transformers huggingface package](Fine-tuning a BERT model)_. 

The current example uses [mBert](https://github.com/google-research/bert/blob/master/multilingual.md) but could be easily adapted for other models supported by the _transformers package_. 

Based on code developed by [Muniza](https://gitlab.wikimedia.org/mnz)
